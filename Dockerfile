
# A simple Dockerfile for a RoR application

FROM ruby:2.5

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs && gem install bundler 
RUN mkdir /endpoint
WORKDIR /endpoint

ADD Gemfile /endpoint/Gemfile
ADD Gemfile.lock /endpoint/Gemfile.lock

RUN bundle install
ADD . /endpoint