class WebhooksController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:receive]
  require 'json'
  def receive
    if request.headers['Content-Type'] == 'application/json'
      data = JSON.parse(request.body.read)
      l = Event.where(sensor_id: data['device']).last

      Event.create!(
      payload: data,
      value: data['count'].max,
      sensor_id: data['device'],
      dbv: data['count'].max - (l.nil? ? 0 : l.value ),
      read_date: data['date'],
      battery: data['battery'],
      temperature: data['temperature']
      )
      
      render json: {"status": "ok"}, status: 200
    else
      # application/x-www-form-urlencoded
      data = params.as_json
    end
  end
end