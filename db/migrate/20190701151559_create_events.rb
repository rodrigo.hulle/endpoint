class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :sensor_id
      t.string :payload
      t.string :name

      t.timestamps
    end
  end
end
