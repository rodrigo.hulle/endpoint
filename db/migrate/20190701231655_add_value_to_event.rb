class AddValueToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :value, :int
    add_column :events, :lat, :float
    add_column :events, :long, :float
    add_column :events, :dbv, :int
  end
end
