class AddBatteryToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :read_date, :datetime
    add_column :events, :battery, :float
    add_column :events, :temperature, :float
  end
end
